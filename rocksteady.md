# Deployment of Cards
After deploying cards, make sure to tag the respective QA members in the respective project's channels about the card or cards that you have just finished deploying to the respective environment.

The deployment process is the standard for this type of project: cherry-pick the necessary branches from the features ready to be deployed, if cherry-picking is needed. I add the card's names and links in the respective PR to make use of it as reference later on.

If there are other deployments or tasks at hand, a batch of cards can be deployed in a single go without cherry-picking, I try to do so.

#  Instance Notes
- The Kafka (not the Kafka Connectors) Instances were set up manually. Each instance per environment runs Zookeeper, Kafka, and Debezium. They are added in Gorilla Stack, and these services are ensured to be running continuously due to systemd unit files. For a quick look into this, you can read this post regarding [Systemd Service Files](https://www.devdungeon.com/content/creating-systemd-service-files).
- The Ubuntu VPN instance was set up with [this script](https://github.com/Nyr/openvpn-install). It uses OpenVPN, and it is used to access the Informix database that the client uses.

# Migration Process
The migration process is only done when there are changes to the datalayer, or the devs indicate there's a need to update the existing data as it's been a long time since the past migration.

**NOTE:** If the migration scripts fail, returning only 499 or 504 status codes, then you can either ask the client to check the Informix database, reboot or maintain it, or you can alternatively copy the existing data from one of the most updated environments to the environment that needs the migration/update. `pg_dump` or the `COPY` command for PostgreSQL are ideal for this. For futher reading [pg_dump](https://www.a2hosting.com/kb/developer-corner/postgresql/import-and-export-a-postgresql-database#Method-1.3A-Use-the-pg-dump-program) and [COPY](https://til.codes/using-postgres-copy-command-to-copy-data-from-one-server-to-another/), follow the respective links. Use the **rocksteady-migrator** EC2 instance for this, as the bandwidth needed for these specific types of migrations can be very consuming, effectively crippling your network and stopping all existing work or communication for you.

**NOTE 2:** If ES Setting and Mappings are changed. Go to Step 11.

1. Delete the data in the tables (at the very least `Inventory`) from the PostgreSQL DB.
2. Migrate the data to the Postgre DB with the scripts. The order of the migration scripts must be:
    - `migration.sh`
    - `models.sh`
    - Any order for the inventory scripts: attachments, cranes, dismantled, equipment,industry, trailers, trucks, and parts. I recommend running parts for last, as it contains the most records.
    - `inspection.sh`
    - The aliases scripts, spammers, and videos in whichever order you choose. 
    - `zipcodes.sh` isn't really executed, but it must be run if the data from it is deleted for any reason or strange situation that may arise.
3. Delete the Debezium container in the Kafka instance.
4. Delete the Kafka topics in the Kafka instance.
5. Close and delete the Elasticsearch (ES) index.
6. Make sure the respective kafka-connector environment has no running instances (modify the auto-scaling group if needed).
7. Start the Debezium container with `sudo systemctl start debezium`.
8. Create the Debezium connector with the POST call.
9. Create the ES inventory index.
10. Restart the kafka-connector (AKA consumer) application.
------------
11. Check if there is a backup index created with the "List Index ES" postman call.
12. If so, go to step 13. If not, create a backup index named with "-backup" suffix in the url (i.e. /invetory-backup). This is done with the postman call "Create ES Index".
13. Run the postman call "ES Reindex", with body source "inventory" -> dest "inventory backup".



# Repo Notes
- [Datalayer](https://github.com/AcklenAvenue/rocksteady-datalayer): remember to follow the migration process when this repository receives new commits.
- [Playbooks](https://github.com/AcklenAvenue/rocksteady-playbooks): remember to ask the client devs to send you the PRs to you to code review them, in order to avoid unwanted errors in the Elasticsearch cluster. If you need any changes regarding Elasticsearch's cluster configuration, please check their [docs](https://www.elastic.co/guide/en/elasticsearch/reference/6.5/docker.html).
- [Kafka Connectors (AKA Consumer)](https://github.com/AcklenAvenue/rocksteady-kafka-connectors): this is the consumer that's in the final migration step. Be sure to shut them down after you've finished migrating all records to their respective environments. Also, this repository has a nice detailed wiki that serves as a good rundown for the services being used.
- [Image Handler Service](https://github.com/AcklenAvenue/rocksteady-image-handler-service): this one rarely sees changes. However, if you need to make a deployment, you'll have to look into how [ClaudiaJS](https://www.claudiajs.com/) is being used. This is an application used to help the [Serverless Image Handler v3.1](https://aws.amazon.com/solutions/serverless-image-handler/) that's used for the images that are presented in the search page. There's a single function deployed for dev and staging and another one for the production account. The Serverless Image Handler is the same one for all environments, and it was only deployed in the production account after AWS made a change in their lambda environments, forcing us to update from v3.0 to v3.1 (v4.0 does not contain the same functionality needed for the current search page).

# Eliseo Questions
1. **Name of instances in EC2 configured manually**

 - rocksteady-kafka-staging 
 - rocksteady-kafka-development
 - rocksteady-ubuntu-vpn

2. **What are the tables to clean?**
-  The scripts for these tables have been uploaded in `tribes-knowledge/green-owl/rocksteady/migration-scripts/sql-scripts/`.

3. **What are the scripts to migrate the data?***
-  The scripts for these tables have been uploaded in `tribes-knowledge/green-owl/rocksteady/migration-scripts/standard-migration-scripts/` or just `tribes-knowledge/green-owl/rocksteady/migration-scripts/`.

4. **How to delete the Debezium container?**
- Esto es un container de Docker, se borra como un container normal de Docker `docker stop container-id; docker rm container-id`.
- La instancia es la que se menciona en la pregunta 1, es un servicio que esta corriendo en las instancias manuales de rocksteady-kafka.
- `docker rm [OPTIONS] CONTAINER [CONTAINER...]`
- [https://docs.docker.com/engine/reference/commandline/rm/](https://docs.docker.com/engine/reference/commandline/rm/)

5. **How to delete the Kafka topics?**
- `cd` into the kafka directory in the kafka instance.
- `./bin/kafka-topics.sh --zookeeper localhost:2181 --delete --topic kafka.*,rocksteady.*`
-  topics: indican qué datos se van a transferir. 

6. **How to close and delete ES (Elastic Search) index?**
- Es usando Postman. Hay que darle import en Postman a la coleccion que mando Raim.[**Rocksteady.postman_collection.json**]  Me voy a collection en Postman y alli selecciono el DELETE DEV ES INDEX. **IMPORTANTE** Esto es parte del proceso de las migraciones, que se encuentra detallado arriba. 

7.  **Steps to stop Kafka-connector instances and how to check if there are instances running?**
- Esto se hace en EB. Es de poner en 0 el auto scaling group. Configuration -> Capacity

8. **Where is the Debezium service to start it?**
-  `sudo systemctl start debezium` Este comando se utiliza en las migraciones, paso 7. 

9. **What and where is the Post call?**
-  Esta en las collecion que se necesita correr en Postman.  `Create Dev Connector`

10. **How to create the ES index?**
- Correr en Postman `Create DEV ES index` si fuera para DEV

11. **How to restar the Kafka-connector?**
- Lo contrario de la respuesta 7, es de cambiar el 0 por 1  en auto scaling.

12. **Is there any diagram that represent the flow of the application?**
- Necesitamos confirmar que esto sea correcto: [https://github.com/AcklenAvenue/rocksteady-kafka-connectors/wiki/Kafka-Architecture](https://github.com/AcklenAvenue/rocksteady-kafka-connectors/wiki/Kafka-Architecture)