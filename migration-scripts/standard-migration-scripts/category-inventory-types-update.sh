export DATALAYER_HOST="rocksteady-datalayer-dev.acklenavenueclient.com"

if [[ "$ENVIRONMENT" == "staging" ]] then
	export DATALAYER_HOST="rocksteady-datalayer-staging.acklenavenueclient.com"
elif [[ "$ENVIRONMENT" == "prod" ]] then
	export DATALAYER_HOST="rocksteady-datalayer-production.e7vsuj2m3h.us-east-1.elasticbeanstalk.com"
fi

curl -X GET --header 'Accept: text/html' "http://$DATALAYER_HOST/category/inventory-types/update"