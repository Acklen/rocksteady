export DATALAYER_HOST="rocksteady-datalayer-dev.acklenavenueclient.com"

if [[ "$ENVIRONMENT" == "staging" ]] then
	export DATALAYER_HOST="rocksteady-datalayer-staging.acklenavenueclient.com"
elif [[ "$ENVIRONMENT" == "prod" ]] then
	export DATALAYER_HOST="rocksteady-datalayer-production.e7vsuj2m3h.us-east-1.elasticbeanstalk.com"
fi

for i in {1..14}; do
	echo -e "Page: $i\n"
	curl -X GET --header 'Accept: application/json' "http://$DATALAYER_HOST/aliases/makes?page=$i&size=1000"
	echo -e "\n"
done
