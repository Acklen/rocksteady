export DATALAYER_HOST="rocksteady-datalayer-dev.acklenavenueclient.com"

if [[ "$ENVIRONMENT" == "staging" ]] then
	export DATALAYER_HOST="rocksteady-datalayer-staging.acklenavenueclient.com"
elif [[ "$ENVIRONMENT" == "prod" ]] then
	export DATALAYER_HOST="rocksteady-datalayer-production.e7vsuj2m3h.us-east-1.elasticbeanstalk.com"
fi

for i in {1..1098}; do
    echo -e "Page: $i\n"
    curl -X PUT --header 'Accept: text/html' "http://$DATALAYER_HOST/products/PARTS/$i/2000"    
    echo -e "\n"
done
